package com.caltail.ecommerce.repository;

import com.caltail.ecommerce.domain.entity.CommentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<CommentEntity, Long> {
    public List<CommentEntity> findAllByProductId(String productId);
}
