package com.caltail.ecommerce.repository;

import com.caltail.ecommerce.domain.entity.TransactionProductEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionProductRepository extends CrudRepository<TransactionProductEntity, String> {

}
