package com.caltail.ecommerce.repository;

import com.caltail.ecommerce.domain.entity.ProductEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, String> {
    // Spring Data JPA
    public ProductEntity findFirstByProductId(String productId);
    // First: only return the first matched record by the given id
    // e.g. productId = 1;
    // SELECT * FROM Product WHERE product_id = 1 LIMIT 1;

    // JPQL, (general syntax for all type of database)
    @Query("SELECT p FROM ProductEntity p WHERE p.productId = :productId")
    ProductEntity findByProductIdMethod2(@Param("productId") String productId);

    // Native Query (Raw Syntax for specified database)
    @Query( // ?1: first params
            value = "SELECT p FROM ProductEntity p WHERE p.productId = :productId LIMIT 1",
            nativeQuery = true)
    ProductEntity findFirstByProductIdMethod3(String productId);
    // Sequence of use: Spring Data JPA => JPQL => Native
    // JPA use prepare statement to create SQL statement to avoid SQL Injection




    List<ProductEntity> findByProductIdIn(List<String> productIds);
    // e.g. productIds = [1,3,5,...];
    // SELECT * FROM Product WHERE product_id IN (1,3,5,...);
}
