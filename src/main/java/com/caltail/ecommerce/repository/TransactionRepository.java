package com.caltail.ecommerce.repository;

import com.caltail.ecommerce.domain.entity.TransactionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends CrudRepository<TransactionEntity, Long> {
    public TransactionEntity findFirstByTransactionId(Long transactionId);
}
