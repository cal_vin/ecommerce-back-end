package com.caltail.ecommerce;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component // Spring bean
public class FirebaseSecurityFilter extends OncePerRequestFilter /* => by config in SecurityConfig, Server will run the filter per request received before calling Api @RestController*/ {
    private Logger log = LoggerFactory.getLogger(FirebaseSecurityFilter.class);

    // Auto call the method doFilterInternal
    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        verifyToken(request);
        filterChain.doFilter(request, response);
        // => run all filter, then flow to the Api @RestController side,
        /*
        pass the PreAuthenticatedAuthenticationToken to Api
        @GetMapping("/me/details")
        public UserDetailsResponseDto getMyUserDetails(Principal principal) ...
         */
    }

    private void verifyToken(HttpServletRequest request) {
        FirebaseToken decodedToken = null;
        String token = getBearerToken(request); // Get access token

        // https://firebase.google.com/docs/auth/admin/verify-id-tokens?authuser=0#
        // decode the idToken by firebase Public key (retrieve from firebase_config.json https://www.googleapis.com/oauth2/v1/certs
        try {
            if (token != null) {
                decodedToken =
                        FirebaseAuth.getInstance().verifyIdToken(token); // decode firebase token into information (Java Object),
                // let firebase SDK verify the token is from firebase, also verify whether expire the token
                // reference: mimi Decode: JWT: https://jwt.io/
            }
        } catch (FirebaseAuthException e) {
            // Invalid IdToken
            e.printStackTrace();
            log.error("Firebase Exception", e);
        }

        // tell Spring to admit the token is validated
        if (decodedToken != null) {
            PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(decodedToken, token, null);
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            // Set/Pass the validated token to Spring Boot, each request will have a Principle contain user Info.
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }

    private String getBearerToken(HttpServletRequest request) {
        String bearerToken = null;
        // getUserIdToken from Request Header !!!
        String authorization =
                request.getHeader("Authorization");
        if (authorization != null && !authorization.isEmpty() &&
                authorization.startsWith("Bearer ")) {
            bearerToken = authorization.substring(7);
        }
        return bearerToken;
        // return null if not login,
    }
}

