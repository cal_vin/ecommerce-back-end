package com.caltail.ecommerce.service;

import com.caltail.ecommerce.domain.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ProductService {
    public ArrayList<Product> getAllProducts();
    public Product getProductDetails(String productId);
    public Map<String, Product> fetchProductsByIds(List<String> productIds);
}
