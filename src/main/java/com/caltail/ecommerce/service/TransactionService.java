package com.caltail.ecommerce.service;

import com.caltail.ecommerce.domain.BillingInformation;
import com.caltail.ecommerce.domain.CheckoutItem;
import com.caltail.ecommerce.domain.Transaction;

import java.util.List;

public interface TransactionService {
    public Transaction createTransaction(List<CheckoutItem> checkoutItems);
    public Transaction getTransactionById(Long transactionId);
    public Transaction completeTransaction(Long transactionId,  BillingInformation billingInformation);
}
