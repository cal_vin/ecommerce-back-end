package com.caltail.ecommerce.service;

import com.caltail.ecommerce.domain.Comment;

import java.util.List;

public interface CommentService {
    public Comment saveComment(Comment comment);
    public List<Comment> getAllComments(String productId);
}
