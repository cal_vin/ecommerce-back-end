package com.caltail.ecommerce.service.impl;

import com.caltail.ecommerce.domain.BillingInformation;
import com.caltail.ecommerce.domain.CheckoutItem;
import com.caltail.ecommerce.domain.Transaction;
import com.caltail.ecommerce.domain.TransactionItem;
import com.caltail.ecommerce.domain.entity.ProductEntity;
import com.caltail.ecommerce.domain.entity.TransactionEntity;
import com.caltail.ecommerce.domain.entity.TransactionProductEntity;
import com.caltail.ecommerce.domain.enumeration.TransactionStatusEnum;
import com.caltail.ecommerce.exception.ValidationException;
import com.caltail.ecommerce.repository.TransactionProductRepository;
import com.caltail.ecommerce.repository.TransactionRepository;
import com.caltail.ecommerce.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Primary // when @Autowired TransactionService Interface in API,
// tell Spring boot choose this impl first if more than one class implement TransactionService Interface
public class TransactionServiceImpl implements TransactionService {
    Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Autowired
    private ProductServiceImpl productService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionProductRepository transactionProductRepository;

    @Override
    public Transaction createTransaction(List<CheckoutItem> checkoutItems) {

        // ### Get [ProductEntity] by productIds List from checkoutItems
        List<String> productIds = new ArrayList<>();

        for (int i = 0; i < checkoutItems.size(); i++) {
            productIds.add(checkoutItems.get(i).getProductId());
        }

        // Transaction Service shouldn't call productRepositoryImpl directly,
        // Retrieve Product entities by productServiceImpl
        List<ProductEntity> products = productService.getProductEntityByIds(productIds);


        logger.debug("products:\n" + products);

        // ### combine quantity from checkoutItem + price from entity together
        Map<CheckoutItem, ProductEntity> checkoutItemProductEntityMap = new HashMap<>();

        for (ProductEntity product : products) {
            for (CheckoutItem checkoutItem : checkoutItems) {
                if ( product.getProductId().equals(checkoutItem.getProductId()) ) {
                    checkoutItemProductEntityMap.put(checkoutItem, product);
                    break;
                }
            }
        }

        logger.debug("Map:\n" + checkoutItemProductEntityMap);

        // ### TransactionEntity
        // Calculate total
        double totalPrice = 0;
        for (CheckoutItem checkoutItem : checkoutItemProductEntityMap.keySet()) {
            double subTotal = checkoutItem.getQuantity() * checkoutItemProductEntityMap.get(checkoutItem).getPrice();
            totalPrice += subTotal;
        }

        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setTotal(totalPrice);
        transactionEntity.setStatus(TransactionStatusEnum.initiated);

        // Save TransactionEntity return generated transactionId automatically
        transactionEntity = transactionRepository.save(transactionEntity);


        // ### TransactionProductEntity
        //  Map([{productId, quantity} : productEntity]) + transactionEntity => TransactionProductEntity, and save
        List<TransactionProductEntity> transactionProductEntityList = new ArrayList<>();
        for (CheckoutItem checkoutItem : checkoutItemProductEntityMap.keySet()) {
            TransactionProductEntity transactionProductEntity =
                    new TransactionProductEntity(
                            checkoutItemProductEntityMap.get(checkoutItem), // ProductEntity
                            transactionEntity,
                            checkoutItem.getQuantity()
                    );
            transactionProductEntity = transactionProductRepository.save(transactionProductEntity);
            transactionProductEntityList.add(transactionProductEntity);
        }


        // ### TransactionItems
        // TransactionProductEntityList => transactionItems
        List<TransactionItem> transactionItems = convertToTransactionItems(transactionProductEntityList);

        // ### Transaction
        // Transaction entity + TransactionItem => Transaction DO
        return new Transaction(transactionEntity,transactionItems);
    }

    @Override
    public Transaction getTransactionById(Long transactionId) {
        TransactionEntity transactionEntity = transactionRepository.findFirstByTransactionId(transactionId);

        if (transactionEntity == null) {
            // Error
            throw new ValidationException("Transaction ID not found");
        }

        // ### transactionProductEntityList
        List<TransactionProductEntity> transactionProductEntityList = transactionEntity.getTransactionProductEntities();

        return new Transaction(
                transactionEntity,
                convertToTransactionItems(transactionProductEntityList)
        );
    }

    @Override
    public Transaction completeTransaction(Long transactionId,  BillingInformation billingInformation) {
        TransactionEntity transactionEntity = transactionRepository.findFirstByTransactionId(transactionId);

        if (transactionEntity == null) {
            // Error
            throw new ValidationException("Transaction ID not found");
        }

        // ### TransactionItem[]
        List<TransactionItem> transactionItems = convertToTransactionItems(transactionEntity.getTransactionProductEntities());

        transactionEntity.setFirstName(billingInformation.getFirstName());
        transactionEntity.setLastName(billingInformation.getLastName());
        transactionEntity.setEmail(billingInformation.getEmail());
        transactionEntity.setBillingAddress(billingInformation.getBillingAddress());
        transactionEntity.setStatus(TransactionStatusEnum.success);
        transactionEntity = transactionRepository.save(transactionEntity);

        // ### Transaction
        // TransactionEntity + TransactionItem[] => Transaction DO
        return new Transaction(transactionEntity,transactionItems);
    }

    public List<TransactionItem> convertToTransactionItems(List<TransactionProductEntity> transactionProductEntityList) {
        List<TransactionItem> transactionItems = new ArrayList<>();
        for (TransactionProductEntity transactionProductEntity : transactionProductEntityList) {
            TransactionItem transactionItem = new TransactionItem(transactionProductEntity);
            transactionItems.add(transactionItem);
        }
        return transactionItems;
    }


}
