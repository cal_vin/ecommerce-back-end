package com.caltail.ecommerce.service.impl;

import com.caltail.ecommerce.domain.Product;
import com.caltail.ecommerce.domain.entity.ProductEntity;
import com.caltail.ecommerce.repository.ProductRepository;
import com.caltail.ecommerce.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    // fetchAllProduct Api
    @Override
    public ArrayList<Product> getAllProducts() {
        List<ProductEntity> productEntities = (ArrayList<ProductEntity>) productRepository.findAll();
        ArrayList<Product> products = new ArrayList<>();
        for (int i = 0; i < productEntities.size(); i++) {
            products.add(i, new Product(productEntities.get(i)));
        }

        return products;
    };

    // fetchProductDetails Api
    @Override
    public Product getProductDetails(String productId) {
        return new Product(productRepository.findFirstByProductId(productId));
    };

    // fetchProductByIds Api
    // Enhanced Version: fetchProductByIds Api
    @Override
    public Map<String, Product> fetchProductsByIds(List<String> productIds) {
        Map<String, Product> shoppingCartItems = new HashMap<>();

        List<ProductEntity> matchedProducts = productRepository.findByProductIdIn(productIds);
        for (int i = 0; i < matchedProducts.size(); i++) {
            shoppingCartItems.put(matchedProducts.get(i).getProductId(), new Product(matchedProducts.get(i)));
        }

        return shoppingCartItems;
    }
    // Non-override method, need type casting if want to use the method in @RestController
    public Map<String, Product> fetchProductsByIdsV2(List<String> productIds) {
        Map<String, Product> shoppingCartItems = new HashMap<>();

        for (int i = 0; i < productIds.size(); i++) {
            Product shoppingCartItem = new Product(productRepository.findFirstByProductId(productIds.get(i)));
            shoppingCartItems.put(productIds.get(i), shoppingCartItem);
        }

        return shoppingCartItems;
    }



    // Non-override method, Provide for Transaction Service Impl
    public List<ProductEntity> getProductEntityByIds(List<String> productIds) {
        return productRepository.findByProductIdIn(productIds);
    }

    // Non-override method, Provide for Comment Service Impl
    public ProductEntity getProductEntityById(String productId) {
        return productRepository.findFirstByProductId(productId);
    }


}
