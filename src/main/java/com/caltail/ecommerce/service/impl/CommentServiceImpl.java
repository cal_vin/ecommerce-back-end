package com.caltail.ecommerce.service.impl;

import com.caltail.ecommerce.api.ProductApi;
import com.caltail.ecommerce.domain.Comment;
import com.caltail.ecommerce.domain.entity.CommentEntity;
import com.caltail.ecommerce.domain.entity.ProductEntity;
import com.caltail.ecommerce.repository.CommentRepository;
import com.caltail.ecommerce.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    Logger logger = LoggerFactory.getLogger(ProductApi.class);

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ProductServiceImpl productService;

    @Override
    public Comment saveComment(Comment comment) {
        ProductEntity productEntity = productService.getProductEntityById(comment.getProductId());
        CommentEntity commentEntity = new CommentEntity();
        commentEntity.setProductEntity(productEntity);
        // save() wouldn't return the extra productId, set manually if want it from save()
        commentEntity.setProductId(productEntity.getProductId());

        commentEntity.setWriterName(comment.getWriterName());
        commentEntity.setContent(comment.getContent());
        commentEntity.setTime(comment.getTime());
        commentEntity = commentRepository.save(commentEntity);
        logger.debug("Saved content: " + commentEntity.getContent());

        // save() wouldn't return the extra productId, set manually if want it from save()
        logger.debug("productId: " + commentEntity.getProductId());
        logger.debug("productId from Entity: " + commentEntity.getProductEntity().getProductId());
        return new Comment(commentEntity);
    }

    @Override
    public List<Comment> getAllComments(String productId) {
        List<Comment> comments = new ArrayList<>();
        List<CommentEntity> commentEntities = commentRepository.findAllByProductId(productId);
        for (CommentEntity commentEntity : commentEntities) {
            comments.add(new Comment(commentEntity));
        }
        return comments;
    }


}
