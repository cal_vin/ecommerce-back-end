package com.caltail.ecommerce.domain;

import com.caltail.ecommerce.domain.entity.TransactionEntity;
import com.caltail.ecommerce.domain.enumeration.TransactionStatusEnum;

import java.util.List;

public class Transaction {
    private Long transactionId;

    private List<TransactionItem> items;

    private Double total;
    private TransactionStatusEnum status;


    public Transaction(TransactionEntity transactionEntity, List<TransactionItem> transactionItems) {
        this.transactionId = transactionEntity.getTransactionId();
        this.items = transactionItems;
        this.total = transactionEntity.getTotal();
        this.status = transactionEntity.getStatus();
    }


    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public List<TransactionItem> getItems() {
        return items;
    }

    public void setItems(List<TransactionItem> items) {
        this.items = items;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public TransactionStatusEnum getStatus() {
        return status;
    }

    public void setStatus(TransactionStatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId=" + transactionId +
                ", items=" + items +
                ", total=" + total +
                ", status=" + status +
                '}';
    }
}

