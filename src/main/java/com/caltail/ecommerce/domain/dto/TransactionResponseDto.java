package com.caltail.ecommerce.domain.dto;

import com.caltail.ecommerce.domain.Transaction;
import com.caltail.ecommerce.domain.enumeration.TransactionStatusEnum;

import java.util.ArrayList;
import java.util.List;

public class TransactionResponseDto {
    private Long transactionId;
    private List<TransactionItemResponseDto> items;
    private Double total;
    private TransactionStatusEnum status;

    public TransactionResponseDto(Transaction transaction) {
        this.transactionId = transaction.getTransactionId();

        List<TransactionItemResponseDto> itemDtoList = new ArrayList<>();
        for (int i = 0; i < transaction.getItems().size(); i++) {
            itemDtoList.add(new TransactionItemResponseDto(transaction.getItems().get(i)));
        }

        this.items = itemDtoList;
        this.total = transaction.getTotal();
        this.status = transaction.getStatus();
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public List<TransactionItemResponseDto> getItems() {
        return items;
    }

    public void setItems(List<TransactionItemResponseDto> items) {
        this.items = items;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public TransactionStatusEnum getStatus() {
        return status;
    }

    public void setStatus(TransactionStatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TransactionResponseDto{" +
                "transactionId=" + transactionId +
                ", items=" + items +
                ", total=" + total +
                ", status=" + status +
                '}';
    }


}
