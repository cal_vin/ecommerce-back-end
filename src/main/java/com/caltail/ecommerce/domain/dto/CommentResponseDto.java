package com.caltail.ecommerce.domain.dto;

import com.caltail.ecommerce.domain.Comment;

public class CommentResponseDto {
    private Long commentId;
    private String productId;
    private String writerName;
    private String content;
    private String time;

    public CommentResponseDto(Comment comment) {
        setCommentId(comment.getCommentId());
        setProductId(comment.getProductId());
        setWriterName(comment.getWriterName());
        setContent(comment.getContent());
        setTime(comment.getTime());
    }

    public Long getCommentId() { return commentId; }

    public void setCommentId(Long commentId) { this.commentId = commentId; }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWriterName() {
        return writerName;
    }

    public void setWriterName(String writerName) {
        this.writerName = writerName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



    @Override
    public String toString() {
        return "CommentResponseDto{" +
                "productId='" + productId + '\'' +
                ", writerName='" + writerName + '\'' +
                ", content='" + content + '\'' +
                ", time='" + time + '\'' +
                ", commentId='" + commentId + '\'' +
                '}';
    }
}
