package com.caltail.ecommerce.domain.dto;

import com.caltail.ecommerce.domain.CheckoutItem;

public class CheckoutItemRequestDto {
    private String productId;

    private Long quantity;

    public CheckoutItem toCheckoutItem() {
        CheckoutItem checkoutItem = new CheckoutItem();
        checkoutItem.setProductId(getProductId());
        checkoutItem.setQuantity(getQuantity());
        return checkoutItem;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CheckoutItemRequestDto{" +
                "productId='" + productId + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
