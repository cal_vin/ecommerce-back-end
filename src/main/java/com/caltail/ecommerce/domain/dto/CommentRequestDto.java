package com.caltail.ecommerce.domain.dto;

import com.caltail.ecommerce.domain.Comment;

public class CommentRequestDto {
    private String productId;
    private String content;
    private String time;

    public Comment toComment(String writerName) {
        Comment comment = new Comment();
        comment.setProductId(getProductId());
        comment.setWriterName(writerName);
        comment.setContent(getContent());
        comment.setTime(getTime());
        return comment;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "CommentRequestDto{" +
                "productId='" + productId + '\'' +
                ", content='" + content + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
