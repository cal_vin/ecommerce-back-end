package com.caltail.ecommerce.domain.dto;

import com.caltail.ecommerce.domain.TransactionItem;

public class TransactionItemResponseDto {
    private ProductResponseDto details;
    private Long quantity;
    private Double subtotal;

    TransactionItemResponseDto(TransactionItem item) {
        this.details = new ProductResponseDto(item.getDetails());
        setQuantity(item.getQuantity());
        setSubtotal(item.getSubtotal());
    }

    public ProductResponseDto getDetails() {
        return details;
    }

    public void setDetails(ProductResponseDto details) {
        this.details = details;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    @Override
    public String toString() {
        return "TransactionItemResponseDto{" +
                "details=" + details +
                ", quantity=" + quantity +
                ", subtotal=" + subtotal +
                '}';
    }
}
