package com.caltail.ecommerce.domain.dto;

import com.caltail.ecommerce.domain.BillingInformation;

public class BillingInformationRequestDto {
    private String firstName;
    private String lastName;
    private String email;
    private String billingAddress;

    public BillingInformation toBillingInformation() {
        BillingInformation billingInformation = new BillingInformation();
        billingInformation.setFirstName(getFirstName());
        billingInformation.setLastName(getLastName());
        billingInformation.setEmail(getEmail());
        billingInformation.setBillingAddress(getBillingAddress());
        return billingInformation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    @Override
    public String toString() {
        return "BillingInformationRequestDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", billingAddress='" + billingAddress + '\'' +
                '}';
    }
}
