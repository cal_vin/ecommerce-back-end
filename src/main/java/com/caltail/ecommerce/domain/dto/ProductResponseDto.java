package com.caltail.ecommerce.domain.dto;

import com.caltail.ecommerce.domain.Product;

public class ProductResponseDto {
    private String id;
    private String name;
    private Double price;
    private String description;
    private String imageUrl;

    public ProductResponseDto(Product product) {
        setId(product.getProductId());
        setName(product.getName());
        setPrice(product.getPrice());
        setDescription(product.getDescription());
        setImageUrl(product.getImageUrl());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "ProductResponseDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

}
