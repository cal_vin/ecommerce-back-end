package com.caltail.ecommerce.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "comment")
public class CommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "comment_id", nullable = false, updatable = false)
    private Long commentId;

    @Column(name = "writer_name", nullable = true)
    private String writerName;

    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "time", nullable = false)
    private String time;


    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private ProductEntity productEntity;

    // the extra product_id will only return when retrieve proactive from database not return from save()
    @Column(name = "product_id", // name should be same as @JoinColumn name, otherwise become a new column
            insertable = false, updatable = false)
    private String productId;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getWriterName() {
        return writerName;
    }

    public void setWriterName(String writerName) {
        this.writerName = writerName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "CommentEntity{" +
                "commentId=" + commentId +
                ", writerName='" + writerName + '\'' +
                ", content='" + content + '\'' +
                ", time='" + time + '\'' +
                ", productEntity=" + productEntity +
                ", productId='" + productId + '\'' +
                '}';
    }
}
