package com.caltail.ecommerce.domain.entity;

import com.caltail.ecommerce.domain.enumeration.TransactionStatusEnum;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "transaction")
public class TransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transaction_id", nullable = false, updatable = false)
    private Long transactionId;

    @Column(name = "total", nullable = false)
    private Double total;

    // TODO Enum
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING) // Make JPA to save the enum name into the column value
    private TransactionStatusEnum status;

    @Column(name = "first_name", nullable = true)
    private String firstName;

    @Column(name = "last_name", nullable = true)
    private String lastName;

    @Column(name = "email", nullable = true)
    private String email;

    // TODO one line or five line, can nullable
    @Column(name = "billing_address", nullable = true)
    private String billingAddress;

    @OneToMany(
            mappedBy = "transaction", // the variable name of TransactionEntity in TransactionProductEntity
            fetch = FetchType.EAGER
    )
    private List<TransactionProductEntity> transactionProductEntities;


    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public TransactionStatusEnum getStatus() {
        return status;
    }

    public void setStatus(TransactionStatusEnum status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<TransactionProductEntity> getTransactionProductEntities() {
        return transactionProductEntities;
    }

    public void setTransactionProductEntities(List<TransactionProductEntity> transactionProductEntities) {
        this.transactionProductEntities = transactionProductEntities;
    }

    @Override
    public String toString() {
        return "TransactionEntity{" +
                "transactionId=" + transactionId +
                ", total=" + total +
                ", status=" + status +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", billingAddress='" + billingAddress + '\'' +
                ", transactionProductEntities=" + transactionProductEntities +
                '}';
    }
}
