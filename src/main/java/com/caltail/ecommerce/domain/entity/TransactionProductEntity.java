package com.caltail.ecommerce.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "transaction_details")
public class TransactionProductEntity {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    private String transactionProductId;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private ProductEntity product;

    @ManyToOne
    @JoinColumn(name = "transaction_id", nullable = false)
    private TransactionEntity transaction;

    @Column(name = "quantity", nullable = false)
    private Long quantity;

    @Column(name = "subtotal", nullable = false)
    private Double subtotal;



    public TransactionProductEntity() {}

    public TransactionProductEntity(ProductEntity product, TransactionEntity transaction, Long quantity) {
        this.product = product;
        this.transaction = transaction;
        this.transactionProductId = transaction.getTransactionId() + "_" + product.getProductId();
        this.quantity = quantity;
        this.subtotal = product.getPrice() * quantity;
    }

    public String getTransactionProductId() {
        return transactionProductId;
    }

    public void setTransactionProductId(String transactionProductId) {
        this.transactionProductId = transactionProductId;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }

    public TransactionEntity getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionEntity transaction) {
        this.transaction = transaction;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    @Override
    public String toString() {
        return "TransactionProductEntity{" +
                "transactionProductId='" + transactionProductId + '\'' +
                ", product=" + product +
                ", transaction=" + transaction +
                ", quantity=" + quantity +
                ", subtotal=" + subtotal +
                '}';
    }
}
