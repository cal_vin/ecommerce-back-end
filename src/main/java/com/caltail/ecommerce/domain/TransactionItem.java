package com.caltail.ecommerce.domain;

import com.caltail.ecommerce.domain.entity.TransactionProductEntity;

public class TransactionItem {
    private Product details;
    private Long quantity;
    private Double subtotal;


    public TransactionItem(TransactionProductEntity transactionProductEntity) {
        setDetails(new Product(transactionProductEntity.getProduct()));
        setQuantity(transactionProductEntity.getQuantity());
        setSubtotal(transactionProductEntity.getSubtotal());
    }


    public Product getDetails() {
        return details;
    }

    public void setDetails(Product details) {
        this.details = details;
    }

    public Long getQuantity() {
        return quantity;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "TransactionItem{" +
                "details=" + details +
                ", subtotal=" + subtotal +
                ", quantity=" + quantity +
                '}';
    }
}

