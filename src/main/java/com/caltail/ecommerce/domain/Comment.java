package com.caltail.ecommerce.domain;

import com.caltail.ecommerce.domain.entity.CommentEntity;

public class Comment {
    private Long commentId;
    private String productId;
    private String writerName;
    private String content;
    private String time;

    public Comment() {}
    public Comment(CommentEntity commentEntity) {
        setCommentId(commentEntity.getCommentId());
        setProductId(commentEntity.getProductEntity().getProductId());
        setWriterName(commentEntity.getWriterName());
        setContent(commentEntity.getContent());
        setTime(commentEntity.getTime());
    };

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWriterName() {
        return writerName;
    }

    public void setWriterName(String writerName) {
        this.writerName = writerName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId='" + commentId + '\'' +
                ", productId='" + productId + '\'' +
                ", writerName='" + writerName + '\'' +
                ", content='" + content + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
