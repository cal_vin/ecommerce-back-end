package com.caltail.ecommerce.domain.enumeration;

public enum TransactionStatusEnum {
    initiated, success, failed
}
