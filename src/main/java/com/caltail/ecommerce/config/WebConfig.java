package com.caltail.ecommerce.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// Enable CORS, let own front-end call our API
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // "/**" means all paths in the API server
                .allowedMethods("GET", "POST", "DELETE", "PATCH", "OPTIONS")
                .allowedOrigins(
                        "http://localhost:3010",
                        "http://e-commerce-cal.s3-website-ap-southeast-1.amazonaws.com"
                ); // "Origin": path site where the request send from
    }
}
