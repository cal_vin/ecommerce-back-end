package com.caltail.ecommerce.config;

import com.caltail.ecommerce.FirebaseSecurityFilter;
import com.caltail.ecommerce.api.ProductApi;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

@Configuration // Spring Bean
@EnableWebSecurity // Initiate Security firewall in Spring
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    Logger logger = LoggerFactory.getLogger(ProductApi.class);

    @Autowired
    private FirebaseSecurityFilter firebaseSecurityFilter;

    @PostConstruct // Run before Server Starter
    public void initFirebase() throws IOException {
        // InputStream help load file in Java
        // Init Firebase need firebase_config.json
        InputStream serviceAccount = new ClassPathResource("firebase_config.json").getInputStream(); // the Class will auto find the config file in resources folder

        FirebaseOptions options = FirebaseOptions.builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .build();

        FirebaseApp.initializeApp(options); // Save your json file into firebaseSDK
        logger.debug("FirebaseApp initialize !");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http
                // enable CORS when authentication
                .cors()
                .and()
                // turn off Spring default http setting
                .httpBasic().disable()
                .authorizeRequests()
                    // set API path start will "/public/" no need to login (no need Principle)
                    .antMatchers("/public/**").permitAll()
                    .anyRequest().authenticated() // Set authorizeRequest setting other requests need authenticated before calling our server Apis (need Principle)

                .and()
                // tell Spring run the firebaseSecurityFilter before access @RestController, add a listener
                .addFilterBefore(
                        firebaseSecurityFilter,
                        UsernamePasswordAuthenticationFilter.class)
                .csrf().disable(); // Cross-site request forgery (csrf can be prevented in Server Side along with front-end web page side)
        // Why here???
    }
}

// Flow: init Server... => initFirebase()
// => receive API requests from front-end => firebaseSecurityFilter authentication  (check whether the idToken is from firebase by private key)
// => authorizeRequests (check whether the request is authorized to the user) => API endpoint...
