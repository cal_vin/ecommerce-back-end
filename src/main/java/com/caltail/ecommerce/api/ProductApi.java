package com.caltail.ecommerce.api;

import com.caltail.ecommerce.domain.Product;
import com.caltail.ecommerce.domain.dto.ProductResponseDto;
import com.caltail.ecommerce.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/public/product")
public class ProductApi {
    Logger logger = LoggerFactory.getLogger(ProductApi.class);

    @Autowired // Automatically apply polymorphism, ProductService productService = new ProductServiceImpl()
    ProductService productService;

    @GetMapping("/all")
    public List<ProductResponseDto> fetchAllProduct() {
        List<Product> products = productService.getAllProducts();
        List<ProductResponseDto> productResponseList = new ArrayList<>();
        for (Product product : products) {
            productResponseList.add(new ProductResponseDto(product));
        }

        return productResponseList;
    }

    @GetMapping
    public ProductResponseDto fetchProductDetails(@RequestParam("productId") String productId) {
        return new ProductResponseDto(productService.getProductDetails(productId));
    }

    @PostMapping
    public Map<String, ProductResponseDto> fetchProductsByIds(@RequestBody List<String> productIds) {
        Map<String, ProductResponseDto> responseItems = new HashMap<>();
        Map<String, Product> productItems = productService.fetchProductsByIds(productIds);

        // productItems.keySet(): Array of Key
        for (String key : productItems.keySet()) {
            responseItems.put(
                key, new ProductResponseDto(productItems.get(key))
            );
        }

        return responseItems;
    }


}
