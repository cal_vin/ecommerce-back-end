package com.caltail.ecommerce.api;

import com.caltail.ecommerce.domain.Comment;
import com.caltail.ecommerce.domain.dto.CommentRequestDto;
import com.caltail.ecommerce.domain.dto.CommentResponseDto;
import com.caltail.ecommerce.service.CommentService;
import com.google.firebase.auth.FirebaseToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class CommentApi {
    Logger logger = LoggerFactory.getLogger(CommentApi.class);

    @Autowired
    CommentService commentService;

    private FirebaseToken getFirebaseTokenFromPrincipal(Principal principal) {
        if (principal instanceof PreAuthenticatedAuthenticationToken) {
            PreAuthenticatedAuthenticationToken preAuthenticated  = (PreAuthenticatedAuthenticationToken) principal;
            return (FirebaseToken) preAuthenticated.getPrincipal();
        }
        return null;
    }

    @PostMapping("/comment/create")
    public CommentResponseDto saveComment(Principal principal, @RequestBody CommentRequestDto commentRequestDto) {
        FirebaseToken firebaseToken = getFirebaseTokenFromPrincipal(principal);

        String writerName = null;
        if (firebaseToken.getName() != null) {
            writerName = firebaseToken.getName();
        } else {
            writerName = firebaseToken.getEmail();
        }

        logger.debug("Writer Name:  " + writerName);
        logger.debug("RequestDto: " + commentRequestDto);

        return new CommentResponseDto(commentService.saveComment(commentRequestDto.toComment(writerName)));
    }

    @GetMapping("/public/comment/all")
    public List<CommentResponseDto> fetchAllCommentOnTheProduct(@RequestParam("productId") String productId) {
        List<Comment> comments = commentService.getAllComments(productId);
        logger.debug("Comment List:" + comments);
        List<CommentResponseDto> commentResponseDtoList = new ArrayList<>();

        for (Comment comment: comments) {
            commentResponseDtoList.add(new CommentResponseDto(comment));
        }
        return commentResponseDtoList;
    }
}
