package com.caltail.ecommerce.api;

import com.caltail.ecommerce.domain.CheckoutItem;
import com.caltail.ecommerce.domain.Transaction;
import com.caltail.ecommerce.domain.dto.CheckoutItemRequestDto;
import com.caltail.ecommerce.domain.dto.CompleteTransactionRequestDto;
import com.caltail.ecommerce.domain.dto.TransactionResponseDto;
import com.caltail.ecommerce.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
// Restful API path name standard
@RequestMapping("/transaction") // context path
public class TransactionApi {
    Logger logger = LoggerFactory.getLogger(TransactionApi.class);

    @Autowired
    private TransactionService transactionService;

    @PostMapping("/create")
    public TransactionResponseDto createTransaction(@RequestBody List<CheckoutItemRequestDto> checkoutItemRequestDtoList) {

        logger.debug("Received json body:\n" + checkoutItemRequestDtoList);

        List<CheckoutItem> checkoutItems = new ArrayList<>();

        for (int i = 0; i < checkoutItemRequestDtoList.size(); i++) {
            checkoutItems.add(i, checkoutItemRequestDtoList.get(i).toCheckoutItem());
        }

        logger.debug("checkoutItems:\n" + checkoutItems);

        Transaction transaction = transactionService.createTransaction(checkoutItems);

        return new TransactionResponseDto(transaction);
    }

    @GetMapping("/get/{transactionId}")
    public TransactionResponseDto getTransactionById(@PathVariable Long transactionId) {
        return new TransactionResponseDto(transactionService.getTransactionById(transactionId));
    }


    // PutMapping provide full info to update the whole record
    @PatchMapping("/complete") // provide partial information to update
    public TransactionResponseDto completeTransaction(@RequestBody CompleteTransactionRequestDto completeTransactionRequestDto) {
        logger.debug("CALL Complete !!!");
        Transaction transaction = transactionService.completeTransaction(
                completeTransactionRequestDto.getTransactionId(),
                completeTransactionRequestDto.getBillingInformation().toBillingInformation());

        return new TransactionResponseDto(transaction);
    }


}
