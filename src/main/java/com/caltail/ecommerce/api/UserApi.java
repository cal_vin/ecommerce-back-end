package com.caltail.ecommerce.api;

import com.caltail.ecommerce.domain.dto.UserInfoResponseDto;
import com.google.firebase.auth.FirebaseToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/user/")
public class UserApi {
    @GetMapping("/info/me")
    // Get Principal from each request auto
    public UserInfoResponseDto getMyUserInfo(Principal principal) {
        FirebaseToken firebaseToken = getFirebaseTokenFromPrincipal(principal);
        UserInfoResponseDto dto = new UserInfoResponseDto();
        dto.setUid(firebaseToken.getUid()); // get User unique Id
        dto.setName(firebaseToken.getName());
        Logger logger = LoggerFactory.getLogger(ProductApi.class);
        logger.debug("userInfo.:\n" + dto);
        return dto;
    }

    private FirebaseToken getFirebaseTokenFromPrincipal(Principal principal) {
        if (principal instanceof PreAuthenticatedAuthenticationToken) {
            PreAuthenticatedAuthenticationToken preAuthenticated  = (PreAuthenticatedAuthenticationToken) principal;
            return (FirebaseToken) preAuthenticated.getPrincipal();
        }
        return null;
    }
}
